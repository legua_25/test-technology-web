<?php 

	function gen_salt() {

		$_CHARS = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789');

		$out = '';
		for ($i = 0; $i < 64; $i++)
			$out .= $_CHARS[rand() % count($_CHARS)];

		return $out;
	}

	if (isset($_POST['passwd'])) {

		$passwd = trim($_POST['passwd']);
		$salt = gen_salt();

		echo 'Salt: ' . $salt . '<br />Hash: ' . hash('sha1', $salt . $passwd . str_rot13($salt)) . '<br />';
	}
?>

<form action="" method="POST">
	<input type="password" name="passwd" /><input type="submit" value="Hash me!" />
</form>
