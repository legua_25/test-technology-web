-- /* Database setup script */
-- /* Verify database existence and create if not. */
CREATE DATABASE IF NOT EXISTS `test_tech2`;
USE `test_tech2`;

-- /* Cleanup */
DROP TABLE IF EXISTS `question`;
DROP TABLE IF EXISTS `assigned_survey`;
DROP TABLE IF EXISTS `option`;
DROP TABLE IF EXISTS `survey`;
DROP TABLE IF EXISTS `question_type`;
DROP TABLE IF EXISTS `admins`;
DROP TABLE IF EXISTS `profile`;
DROP TABLE IF EXISTS `user`;

-- /* User class */
CREATE TABLE IF NOT EXISTS `user` (

	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 

	`username` VARCHAR(32) NOT NULL, 
	`email` VARCHAR(72) NOT NULL, 
	`passwd` CHAR(128) NOT NULL, 
	`salt` CHAR(64) NOT NULL, 
	`created` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
	`last_login` TIMESTAMP DEFAULT CURRENT_TIMESTAMP

) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- /* Profile class */
CREATE TABLE `profile` (

	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,

	`user_id` INT UNSIGNED NOT NULL,
	`first_name` VARCHAR(255),
	`last_name` VARCHAR(512),
	`birth_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `gender` TINYINT(1) NOT NULL,

	CHECK(`gender` = 1 OR `gender` = 2),

  FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE

) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- /* Admins class */
CREATE TABLE IF NOT EXISTS `admins` (

	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,

	`user_id` INT UNSIGNED NOT NULL,
	`granted` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`passwd` CHAR(128) NOT NULL, -- /* Use a second password to login as administrator. */
	`salt` CHAR(64) NOT NULL,

	FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE

) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- /* QuestionType class */
CREATE TABLE IF NOT EXISTS `question_type` (

	`id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 

	`type` VARCHAR(52) NOT NULL, 
	`description` TEXT NOT NULL

) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- /* Survey class */
CREATE TABLE IF NOT EXISTS `survey` (

	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 

	`title` VARCHAR(128) NOT NULL, 
	`description` TEXT NOT NULL, 
	`multiple_attempts` TINYINT(1),
	`synced` TINYINT(1),
	`date_added` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

	CHECK(`multiple_attempts` = 0 OR `multiple_attempts` = 1),
	CHECK(`synced` = 0 OR `synced` = 1)

) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- /* Option class */
CREATE TABLE IF NOT EXISTS `option` (

	`id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,

	`question_id` INT UNSIGNED NOT NULL,
	`flavor_text` VARCHAR(128) NOT NULL, 
	`value` FLOAT NOT NULL, -- /* This exists only in the web branch, not locally. */
	`feedback` VARCHAR(128) NOT NULL,

	FOREIGN KEY (`question_id`) REFERENCES `question`(`id`) ON DELETE CASCADE

) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- /* AssignedSurvey class */
CREATE TABLE IF NOT EXISTS `assigned_survey` (

	`id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 

	`user_id` INT UNSIGNED NOT NULL, 
	`survey_id` INT UNSIGNED NOT NULL, 
	`solved` TINYINT(1),

	CHECK (`solved` = 0 OR `solved` = 1)

) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- /* Question class */
CREATE TABLE IF NOT EXISTS `question` (

	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 

	`survey_id` INT UNSIGNED NOT NULL, 
	`type_id` SMALLINT UNSIGNED NOT NULL, 

	`flavor_text` VARCHAR(128) NOT NULL, 
	`required` TINYINT(1),
	`answer_id` BIGINT UNSIGNED,

	CHECK (`required` = 0 OR `required` = 1),

	FOREIGN KEY (`survey_id`) REFERENCES `survey`(`id`) ON DELETE CASCADE,
	FOREIGN KEY (`type_id`) REFERENCES `question_type`(`id`), 
	FOREIGN KEY (`answer_id`) REFERENCES `option`(`id`)	

) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;
