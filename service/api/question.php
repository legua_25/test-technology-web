<?php

	if (!defined('TEST_TECH'))
		die ('Undefined context root.');

	global $app;

	# Get question type for question ID
	$app->post('/question/:id/type', function ($id) use ($app) {

		$params = tt_post($app, array ( 'key' ));
		$data = null;

		if (!empty($params)) {

			$key = md5(base64_decode(urldecode($params['key'])));

			if ($key == TT_SERVER_KEY)
				$data = tt_get_question_type_for_id($id);
		}

		echo json_encode($data);
	});

	# Get question type by ID
	$app->post('/question/types/:id', function ($id) use ($app) {

		$params = tt_post($app, array ( 'key' ));
		$data = null;

		if (!empty($params)) {

			$key = md5(base64_decode(urldecode($params['key'])));

			if ($key == TT_SERVER_KEY)
				$data = tt_get_question_type($id);
		}

		echo json_encode($data);
	});

	# Get all question types registered
	$app->post('/question/types', function () use ($app) {

		$params = tt_post($app, array ( 'key' ));
		$data = null;

		if (!empty($params)) {

			$key = md5(base64_decode(urldecode($params['key'])));

			if ($key == TT_SERVER_KEY)
				$data = tt_get_all_question_types();
		}

		echo json_encode($data);
	});

	# Deserialize question/options bundle
	$app->post('/question/add', function () use ($app) {

		$params = tt_post($app, array ( 'key', 'data' ));
		$data = null;

		if (!empty($params)) {

			$key = md5(base64_decode(urldecode($params['key'])));
			$json_data = $params['data'];

			if ($key == TT_SERVER_KEY)
				$data = tt_deserialize_question($json_data);
		}

		echo json_encode($data);
	});

?>
