<?php

	if (!defined('TEST_TECH'))
		die ('Undefined context root.');

	global $app;

	# Authenticate server
	$app->post('/server/auth', function () use ($app) {

		$params = tt_post($app, array ( 'device-id', 'build-no', 'server-user', 'server-passwd' ));
		$data = null;

		if (!empty($params)) {
			$username = trim($params['server-user']);
			#$passwd = hash('sha1', base64_decode($params['server-passwd']));
			$passwd = base64_decode($params['server-passwd']);
			#var_dump($params);
			#echo 'VS SVR: '.TT_SERVER_PASS.' : AD: '.$passwd.' ';
			if ($username == TT_SERVER_USER && $passwd == TT_SERVER_PASS)
				$data = array ( 'date-registered' => time(), 'server-key' => urlencode(TT_SERVER_KEY) );
		}

		echo json_encode($data);
	});

?>
