<?php

	if (!defined('TEST_TECH'))
		die ('Undefined context root.');

	global $app;

	# Authenticate user
	$app->post('/user/auth/:user', function ($username) use ($app) {

		$params = tt_post($app, array ( 'server-key', 'passwd' ));
		$data = null;

		if (!empty($params)) {

			$key = base64_decode(urldecode($params['server-key']));
			$passwd = base64_decode(urldecode($params['passwd']));

			if ($key == TT_SERVER_KEY) {
				$user = array('id' => 1, 'username' => '_root', 'email' => 'me@example.com', 'created' => 500);
				#$user = tt_authenticate($username, $passwd);
				if (!empty($user))
					$data = $user;
			}
		}

		echo json_encode($data);
	});

	# Add user
	$app->post('/user/add', function () use ($app) {

		$params = tt_post($app, array ( 'key', 'user', 'email', 'passwd' ));
		$data = null;

		if (!empty($params)) {

			$key = md5(base64_decode(urldecode($params['key'])));
			$user = trim($params['user']);
			$email = trim($params['email']);
			$passwd = base64_decode(urldecode($params['passwd']));

			if ($key == TT_SERVER_KEY) {

				if (!empty($user) && !empty($email) && !empty($passwd)) {

					$salt = tt_salt();
					$passwd = tt_hash_password($passwd, $salt);

					$data = tt_new_user(array (

						'username' => $user,
						'email' => $email,
						'passwd' => $passwd,
						'salt' => $salt

					));
				}
			}
		}

		echo json_encode($data);
	});

	# Save user data
	$app->post('/user/save', function () use ($app) {

		$params = tt_post($app, array ( 'key', 'id', 'user', 'passwd' ));
		$data = null;

		if (!empty($params)) {

			$key = base64_decode(urldecode($params['key']));
			$id = $params['id'];
			$user = trim($params['user']);
			$passwd = base64_decode(urldecode($params['passwd']));

			if ($key == TT_SERVER_KEY) {

				$salt = tt_query('SELECT `salt` FROM `user` WHERE `id` = :id', array ( ':id' => $id ));
				$passwd = tt_hash_password($passwd, $salt[0]['salt']);

				$data = tt_set_user($id, array ( 'username' => $user, 'passwd' => $passwd ));
			}
		}

		echo json_encode($data);
	});

	# Delete user data
	$app->post('/user/drop/:user', function ($username) use ($app) {

		$params = tt_post($app, array ( 'key', 'passwd' ));
		$data = null;

		if (!empty($params)) {

			$key = base64_decode(urldecode($params['key']));
			$passwd = base64_decode(urldecode($params['passwd']));

			if ($key == TT_SERVER_KEY) {

				$user = tt_authenticate($username, $passwd);
				if (!empty($user))
					$data = tt_delete_user($username);
			}
		}

		echo json_encode($data);
	});

	# Query user profile
	$app->post('/user/profile/:user_id', function ($id) use ($app) {

		$params = tt_post($app, array ( 'key' ));
		$data = null;

		if (!empty($params)) {

			$key = base64_decode(urldecode($params['key']));

			if ($key == TT_SERVER_KEY) {

				$profile = tt_get_profile($id);
				if (!empty($profile))
					$data = $profile;
			}
		}

		echo json_encode($data);
	});

	# Save user profile
	$app->post('/user/profile/:user_id/save', function ($id) use ($app) {

		$params = tt_post($app, array ( 'key', 'first-name', 'last-name', 'birth-date', 'gender' ));
		$data = null;

		if (!empty($params)) {

			$key = base64_decode(urldecode($params['key']));
			$profile = array (

				'first-name' => $params['first-name'],
				'last-name' => $params['last-name'],
				'birth-date' => $params['birth-date'],
				'gender' => $params['gender']

			);

			if ($key == TT_SERVER_KEY)
				$data = tt_set_profile($id, $profile);
		}

		echo json_encode($data);
	});

?>
