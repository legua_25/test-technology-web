<?php

	if (!defined('TEST_TECH'))
		die ('Undefined context root.');

	global $app;

	# Get all surveys
	$app->post('/surveys/all', function () use ($app) {

		$params = tt_post($app, array ( 'key' ));
		$data = null;

		if (!empty($params)) {

			$key = md5(base64_decode(urldecode($params['key'])));

			if ($key == TT_SERVER_KEY)
				$data = tt_get_all_surveys();
		}

		echo json_encode($data);
	});

	# Get survey list
	$app->post('/surveys/:limit', function ($limit) use ($app) {

		$params = tt_post($app, array ( 'key', 'id', 'last-login' ));
		$data = null;

		if (!empty($params)) {

			$key = md5(base64_decode(urldecode($params['key'])));
			$id = $params['id'];
			$last_login = $params['last-login'];

			if ($key == TT_SERVER_KEY)
				$data = tt_get_surveys_for_user($id, $last_login, $limit);
		}

		echo json_encode($data);
	});

	# Create new survey
	$app->post('/survey/add', function () use ($app) {

		$params = tt_post($app, array ( 'key', 'title', 'description', 'multiple_attempts' ));
		$data = null;

		if (!empty($params)) {

			$key = md5(base64_decode(urldecode($params['key'])));
			$title = $params['title'];
			$description = $params['description'];
			$multiple_attempts = $params['multiple_attempts'];

			if ($key == TT_SERVER_KEY) {

				$data = tt_new_survey(array (

					'title' => $title,
					'description' => $description,
					'multiple_attempts' => $multiple_attempts

				));
			}
		}

		echo json_encode($data);
	});

	# Delete survey
	$app->post('/survey/drop/:id', function ($id) use ($app) {

		$params = tt_post($app, array ( 'key', 'user', 'passwd' ));
		$data = null;

		if (!empty($params)) {

			$key = md5(base64_decode(urldecode($params['key'])));
			$username = $params['user'];
			$passwd = base64_decode(urldecode($params['passwd']));

			if ($key == TT_SERVER_KEY) {

				$user = tt_authenticate($username, $passwd);
				if (!empty($user))
					$data = tt_delete_survey($id);
			}
		}

		echo json_encode($data);
	});

?>
