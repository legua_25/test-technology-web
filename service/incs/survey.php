<?php

	if (!defined('TEST_TECH'))
		die ('Undefined context root.');

	function tt_get_surveys_for_user($id, $last_login, $limit = 5) {

		return tt_query('SELECT `id`, `title`, `description`, `multiple_attempts`
								FROM `survey`
								INNER JOIN `assigned_survey` ON `assigned_survey`.`survey_id` = `survey`.`id`
								WHERE `user_id` = :id AND `synced` = 0 AND `date_added` >= :date
								LIMIT :limit',
							array ( ':id' => $id, ':date' => $last_login, ':limit' => $limit ));
	}

	function tt_get_all_surveys() {

		return tt_query('SELECT `id`, `title`, `description`, `multiple_attempts`
							FROM `survey`');
	}

	function tt_new_survey($data) {

		$query = 'INSERT INTO `survey` VALUES(NULL, ' . array_reduce(array_keys($data), function ($result, $e) {

				if ($result == null)
					return ':' . $e;

				return $result . ', :' . $e;
			}) . ', 0, FROM_UNIXTIME(0))';
		$params = array();
		array_map(function ($key, $value) use (&$params) { $params[':' . $key] = $value; }, array_keys($data), array_values($data));

		return tt_execute($query, $params);
	}

	function tt_set_survey($id, $data) {

		$query = 'UPDATE `survey` SET ' . array_reduce(array_keys($data), function ($result, $key) {

				if ($result == null)
					return '`' . $key . '` = :' . $key;

				return $result . ', `' . $key . '` = :' . $key;
			}) . ' WHERE `id` = :id';
		$params = array ();
		$params[':id'] = $id;
		array_map(function ($key, $value) use (&$params) { $params[':' . $key] = $value; }, array_keys($data), array_values($data));

		return tt_execute($query, $params);
	}

	function tt_delete_survey($id) {

		$query = 'DELETE FROM `survey` WHERE `id` = :id';
		return tt_execute($query, array ( ':id' => $id ));
	}

?>
