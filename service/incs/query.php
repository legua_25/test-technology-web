<?php

	if (!defined('TEST_TECH'))
		die ('Undefined context root.');

	function tt_init() {

		$dsn = 'mysql:host=' . TT_DATABASE_HOST . ';dbname=' . TT_DATABASE_NAME;
		try {

			$db = new PDO($dsn, TT_DATABASE_USER, TT_DATABASE_PASS);
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

			return $db;
		}
		catch (PDOException $e) { return null; }
	}

	function tt_query($query, $params = array ()) {

		try {

			$db = tt_init();
			$stmt = $db->prepare($query);
			$stmt->execute($params);

			return $stmt->fetchAll();
		}
		catch (PDOException $e) { return array (); }
	}

	function tt_execute($query, $params = array ()) {

		try {

			$db = tt_init();
			$stmt = $db->prepare($query);
			return $stmt->execute($params);
		}
		catch (PDOException $e) { return false; }
	}

?>
