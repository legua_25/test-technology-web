<?php

	if (!defined('TEST_TECH'))
		die ('Undefined context root.');

	require_once (TT_BASE_DIR . 'libs/slim/Slim/Slim.php');
	Slim\Slim::registerAutoloader();

	global $app;
	$app = new Slim\Slim();
	$app->contentType('application/json');

	function tt_load_api($base_dir = TT_API_DIR) {

		foreach (scandir($base_dir) as $file) {

			if ($file == '.' || $file == '..')
				continue;

			$path = ($base_dir . $file);
			if (is_dir($path))
				tt_load_api($path . '/');

			$file_data = pathinfo($path);

			if ($file_data['extension'] == 'php')
				require_once ($base_dir . $file);
		}
	}

	function tt_post(Slim\Slim $app, $names) {

		$out = array ();
		foreach ($names as $name) {
			$out[$name] = $app->request()->post($name);
		}
		return $out;
	}

	tt_load_api();

	$app->run();


?>
