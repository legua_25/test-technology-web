<?php

	if (!defined('TEST_TECH'))
		die ('Undefined context root.');

	require_once (TT_INCS_DIR . 'query.php');
	require_once (TT_INCS_DIR . 'user.php');
	require_once (TT_INCS_DIR . 'profile.php');
	#require_once (TT_INCS_DIR . 'admins.php');
	require_once (TT_INCS_DIR . 'survey.php');
	require_once (TT_INCS_DIR . 'question.php');
	#require_once (TT_INCS_DIR . 'option.php');
	require_once (TT_INCS_DIR . 'api.php');

?>
