<?php

	if (!defined('TEST_TECH'))
		die ('Undefined context root.');

	function tt_get_question_type_for_id($id) {

		$type = tt_query('SELECT * FROM `question_type`
		                    INNER JOIN `question` ON `question`.`type_id` = `question_type`.`id`
							WHERE `question`.`id` = :id',
						array ( ':id' => $id ));

		if (!empty($type)) {

			$type = $type[0];
			return $type;
		}

		return null;
	}

	function tt_get_question_type($id) {

		$type = tt_query('SELECT * FROM `question_type` WHERE `id` = :id', array ( ':id' => $id ));

		if (!empty($type)) {

			$type = $type[0];
			return $type;
		}

		return null;
	}

	function tt_get_all_question_types() {
		return tt_query('SELECT * FROM `question_type`');
	}

	function tt_deserialize_question($json_data) {

		$data = json_decode($json_data, true);

		$survey_id = $data['survey-id'];
		$type_id = $data['type-id'];
		$flavor_text = $data['flavor-text'];
		$required = ($data['required'] ? 1 : 0);

		$id = tt_new_question(array (

			'survey_id' => $survey_id,
			'type_id' => $type_id,
			'flavor_text' => $flavor_text,
			'required' => $required

		));

		if ($id != null) {

			$options = $data['option'];

			foreach ($options as $option)
				tt_new_option($option, $id);
		}

		return $id;
	}

	function tt_new_question($data) {

		$query = 'INSERT INTO `question` VALUES(NULL, ' . array_reduce(array_keys($data), function ($result, $e) {

				if ($result == null)
					return ':' . $e;

				return $result . ', :' . $e;
			}) . ', 0)';
		$params = array();
		array_map(function ($key, $value) use (&$params) { $params[':' . $key] = $value; }, array_keys($data), array_values($data));
		$id = null;

		try {

			$db = tt_init();
			$stmt = $db->prepare($query);
			if ($stmt->execute($params))
				$id = $db->lastInsertId();

			unset($db, $stmt);
		}
		catch (PDOException $e) { return null; }

		return $id;
	}

	function tt_new_option($data, $id) {

		$query = 'INSERT INTO `option` VALUES(NULL, :question_id' . array_reduce(array_keys($data), function ($result, $e) {

				if ($result == null)
					return ':' . $e;

				return $result . ', :' . $e;
			}) . ')';
		$params = array();
		$params[':question_id'] = $id;
		array_map(function ($key, $value) use (&$params) { $params[':' . $key] = $value; }, array_keys($data), array_values($data));

		return tt_execute($query, $params);
	}

?>
