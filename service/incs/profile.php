<?php

	if (!defined('TEST_TECH'))
		die ('Undefined context root.');

	function tt_get_profile($id) {

		$profile = tt_query('SELECT `id`, `first_name`, `last_name`, `birth_date`, `gender`
								FROM `profile`
								WHERE `user_id` = :id',
							array ( ':id' => $id ));

		if (!empty($profile))
			$profile = $profile[0];

		return $profile;
	}

	function tt_set_profile($id, $data) {

		$query = 'UPDATE `profile` SET ' . array_reduce(array_keys($data), function ($result, $key) {

				if ($result == null)
					return '`' . $key . '` = :' . $key;

				return $result . ', `' . $key . '` = :' . $key;
			}) . ' WHERE `user_id` = :id';
		$params = array ();
		$params[':id'] = $id;
		array_map(function ($key, $value) use ($params) { $params[':' . $key] = $value; }, array_keys($data), array_values($data));

		return tt_execute($query, $params);
	}

	function tt_new_profile($id, $data) {

		$query = 'INSERT INTO `profile` VALUES(NULL, :user_id, ' . array_reduce(array_keys($data), function ($result, $e) {

				if ($result == null)
					return ':' . $e;

				return $result . ', :' . $e;
			}) . ')';
		$params = array();
		$params[':user_id'] = $id;
		array_map(function ($key, $value) use ($params) { $params[':' . $key] = $value; }, array_keys($data), array_values($data));

		if (tt_execute($query, $params))
			return tt_get_profile($id);

		return array ();
	}

?>
