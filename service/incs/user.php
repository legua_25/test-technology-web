<?php

	if (!defined('TEST_TECH'))
		die ('Undefined context root.');

	function tt_get_user($username) {

		if (tt_execute('UPDATE `user` SET `last_login` = FROM_UNIXTIME(NOW()) WHERE `username` = :username', array ( ':username' => $username ))) {

			$user = tt_query('SELECT `id`, `username`, `email`, UNIX_TIMESTAMP(`created`) AS \'created\', UNIX_TIMESTAMP(`last-sync`) AS \'last-sync\'
								FROM `user`
								WHERE `username` = :username',
				array ( ':username' => $username ));
			$user = $user[0];

			return $user;
		}

		return null;
	}

	function tt_salt($values = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789') {

		$values = str_split($values);
		$size = (count($values) - 1);

		$out = '';
		for ($i = 0; $i < 64; $i++)
			$out .= $values[rand(0, $size)];

		return $out;
	}

	function tt_hash_password($passwd, $salt = null) {

		if ($salt == null)
			$salt = tt_salt();

		return hash('sha1', $salt . $passwd . str_rot13($salt));
	}

	function tt_authenticate($username, $passwd) {

		$passwd_salt = tt_query('SELECT `passwd`, `salt` FROM `user` WHERE `username` = :username', array ( ':username' => $username ));

		if (!empty($passwd_salt)) {

			$passwd_salt = $passwd_salt[0];

			$db_passwd = $passwd_salt['passwd'];
			$salt = $passwd_salt['salt'];

			$passwd = tt_hash_password($passwd, $salt);

			if ($passwd == $db_passwd)
				return tt_get_user($username);
		}

		return array ();
	}

	function tt_set_user($id, $data) {

		$query = 'UPDATE `user` SET ' . array_reduce(array_keys($data), function ($result, $key) {

			if ($result == null)
				return '`' . $key . '` = :' . $key;

			return $result . ', `' . $key . '` = :' . $key;
		}) . ' WHERE `id` = :id';
		$params = array ();
		$params[':id'] = $id;
		array_map(function ($key, $value) use (&$params) { $params[':' . $key] = $value; }, array_keys($data), array_values($data));

		return tt_execute($query, $params);
	}

	function tt_new_user($data) {

		$query = 'INSERT INTO `user` VALUES(NULL, ' . array_reduce(array_keys($data), function ($result, $e) {

			if ($result == null)
				return ':' . $e;

			return $result . ', :' . $e;
		}) . ', FROM_UNIXTIME(NOW()), FROM_UNIXTIME(0))';
		$params = array();
		array_map(function ($key, $value) use (&$params) { $params[':' . $key] = $value; }, array_keys($data), array_values($data));

		if (tt_execute($query, $params))
			return tt_get_user($data['username']);

		return array ();
	}

	function tt_delete_user($username) {

		$query = 'DELETE FROM `user` WHERE `username` = :username';
		return tt_execute($query, array ( ':username' => $username ));
	}

?>
