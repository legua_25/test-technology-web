<?php

	if (!defined('TEST_TECH'))
		die ('Undefined context root.');

	# Define basic constants
	define('TT_VERSION', '1.0');
	define('TT_DATABASE_VERSION', '1.0');
	define('TT_PHP_MIN', '5.3.0');

	# Check PHP version: we cannot run under TT_PHP_MIN
	$current_version = phpversion();
	if (version_compare(TT_PHP_MIN, $current_version, '>')) {

		header('Content-Type: text/html; charset=utf-8');
		die ('This server is currently running version ' . $current_version . ', but this installation of Test Technology (' . TT_VERSION . ') requires version ' . TT_PHP_MIN . ' or greater.');
	}
	
	# Kill the magic quotes stuff
	@ini_set('magic_quotes_runtime', 0);
	@ini_set('magic_quotes_sybase', 0);

	# Define site constants
	define('TT_DATABASE_NAME', 'test_tech2');
	define('TT_DATABASE_USER', 'testtechnology');
	define('TT_DATABASE_PASS', base64_decode('anN5ZTRvcQ=='));
	define('TT_DATABASE_HOST', 'localhost');

	define('TT_SERVER_KEY', md5('server_key'));
	define('TT_SERVER_USER', 'root');
	define('TT_SERVER_PASS', hash('sha1', base64_decode('cGFzcw==')));

	define('TT_BASE_LANG', 'en-us');
	define('TT_DEBUG_MODE', false);

	# Continue the loading process
	require_once (TT_INCS_DIR . 'load.php');

?>
