<?php

	if (!defined('TEST_TECH')) {

		define('TEST_TECH', true);
		define('TT_BASE_DIR', __DIR__ . '/');
		define('TT_INCS_DIR', TT_BASE_DIR . 'incs/');
		define('TT_API_DIR', TT_BASE_DIR . 'api/');
	}

	if (file_exists(TT_INCS_DIR . 'settings_sample.php'))
		$file = file(TT_INCS_DIR . 'settings_sample.php');
	else
		die ('Missing file "settings_sample.php". Cannot run installer without it. Ensure the file is locatable and has read permissions and try again.');

	# TODO: Finish later...

?>
