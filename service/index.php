<?php

	# Define the execution context
	if (!defined('TEST_TECH')) {

		define('TEST_TECH', true);
		define('TT_BASE_DIR', __DIR__ . '/');
		define('TT_INCS_DIR', TT_BASE_DIR . 'incs/');
		define('TT_API_DIR', TT_BASE_DIR . 'api/');
	}

	if (file_exists(TT_BASE_DIR . 'settings.php'))
		require_once (TT_BASE_DIR . 'settings.php');
	else {

		# TODO: Set the call-to-installer code here.
		if (file_exists(TT_BASE_DIR . 'install.php')) {

			require_once (TT_INCS_DIR . 'errors.php');
			tt_settings_missing();
		}
		else
			die ('Script "install.php" is missing. Upload to server and ensure read/write privileges and try again.');
	}

?>