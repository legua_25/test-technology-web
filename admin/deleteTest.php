<?php
	require_once('settings.php');

	if(!isset($_SESSION)) {
		session_start();
	}
	
	if($_POST) {	
		$post_params = '';
		$post_params .= 'key=' .urlencode(base64_encode(TT_SERVER_KEY)) .'&';
		$post_params .= 'user=' .$_SESSION['username'] .'&';
		$post_params .= 'passwd=' .urlencode(base64_encode($_POST['password'])); 

		$url = TT_SERVICE_HOST .'survey/drop/' .$_GET['test'];
	
		$c = curl_init($url);
		curl_setopt($c, CURLOPT_POST, true); 
		curl_setopt($c, CURLOPT_POSTFIELDS, $post_params);
		curl_setopt($c, CURLOPT_HEADER, false); 
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
	
		$response = json_decode(curl_exec($c), true);
		curl_close($c);

		var_dump($response);
		if($response) {
			echo "Test was successfully deleted.";
			include_once('./views/welcome.php');
			exit();
		}
		else {
			echo "Test couldn't be deleted. Try again.";
		}
	
	}
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>Delete test - Test Technology</title>
		<link rel="stylesheet" type="text/css" href="login.css">	
	</head>
	<body>
		<div class="login">
		<p>Please confirm with your password that you really want to delete this test.</p>
		<form method="POST" action="">
			<p>Password: <input type="password" name="password" id="password" required /></p>
			<p><input type="submit" name="delete" id="delete" value="Delete test"></p>
		</form>
	</div>
	</body>
</html>
