var i = 0; // Index of last question created.
var iOpt = [0]; // Index of last option created per question.

function newQuestion() {
	q = document.createElement("div");
	iOpt.push(0);
	i++;
	q.setAttribute("id", "q_"+i);
	question_type = document.getElementById("question_type").value;
	
	content = "<table>";
	content += "<tr><td>Question:</td><td><textarea name=\"question_"+i+"\" id=\"question_"+i+"\" required></textarea></td>";
	content += "<td><button type=\"button\" id=\"remove_"+i+"\" name=\"remove_"+i+"\" onclick=\"removeQuestion("+i+")\">Remove question</button></td></tr>";
	content += "<tr><td>Required:</td><td><input type=\"checkbox\" name=\"required_"+i+"\" id=\"required_"+i+"\"></td></tr>";
	switch(question_type) {
		case "Text":
			content += "<tr><td>Answer:</td><td><input type=\"text\" name=\"option"+i+"_1\" id=\"answer_"+i+"_1\" required></td><tr/>";
			content += "<tr><td>Value:</td><td><input type=\"number\" name=\"value_"+i+"_1\" id=\"value_"+i+"_1\"></td><tr/>";
			content += "<tr><td>Feedback:</td><td><textarea name=\"feedback_"+i+"_1\" id=\"feedback_"+i+"_1\"></textarea></td><tr/>";
			content += "<input type=\"hidden\" name=\"answer_"+i+"\" id=\"answer_"+i+"\" value=\"1\">";
			iOpt[i]++;
		break;
		case "Radio":
			content += "<tr><td><button type=\"button\" name=\"button_"+i+" id=\"button_"+i+" onclick=\"newOption("+i+")\">New option</button></td></tr>";
		break;
	}
	content += "</table>";
	content += "<input type=\"hidden\" name=\"question_type_"+i+"\" id=\"question_type_"+i+"\" value=\""+question_type+"\">";
	q.innerHTML = content;
	document.getElementById("questions").appendChild(q);
}

function newOption(iQ) {
	iOpt[iQ]++;
	content = "<table>";
	content += "<tr><td>Option:</td><td><input type=\"text\" id=\"option"+iQ+"_"+iOpt[iQ]+"\" name=\"option"+iQ+"_"+iOpt[iQ]+"\" required></td>";
	content += "<td><button type=\"button\" id=\"remove_"+iQ+"_"+iOpt[iQ]+"\" name=\"remove_"+iQ+"_"+iOpt[iQ]+"\" onclick=\"removeOption("+iQ+","+iOpt[iQ]+")\">Remove option</button></td></tr>";
	content += "<tr><td>Correct answer:</td><td><input type=\"radio\" name=\"answer_"+iQ+"\" id=\"answer_"+iQ+"\" value=\""+iOpt[iQ]+"\"></td></tr>";
	content += "<tr><td>Value:</td><td><input type=\"number\" name=\"value_"+iQ+"_"+iOpt[iQ]+"\" id=\"value_"+iQ+"_"+iOpt[iQ]+"\"></td><tr/>";
	content += "<tr><td>Feedback:</td><td><textarea name=\"feedback_"+iQ+"_"+iOpt[iQ]+"\" id=\"feedback_"+iQ+"_"+iOpt[iQ]+"\"></textarea></td><tr/>";
	content += "</table>";
	c = document.createElement("div");
	c.setAttribute("id", "opt_"+iQ+"_"+iOpt[iQ]);
	c.innerHTML = content;
	document.getElementById("q_"+iQ).appendChild(c);
}

function removeQuestion(iQ) {
	document.getElementById("questions").removeChild(document.getElementById("q_"+iQ));
}

function removeOption(iQ, iO) {
	document.getElementById("q_"+iQ).removeChild(document.getElementById("opt_"+iQ+"_"+iO));
}
