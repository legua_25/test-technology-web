<?php

	# Define the execution context
	if (!defined('TEST_TECH')) {

		define('TEST_TECH', true);
		define('TT_BASE_DIR', __DIR__ . '/');
	}

	if (file_exists(TT_BASE_DIR . 'settings.php'))
		require_once (TT_BASE_DIR . 'settings.php');
	else {

		# TODO: Set the call-to-installer code here.
		if (file_exists(TT_BASE_DIR . 'install.php')) {

			require_once (TT_INCS_DIR . 'errors.php');
			tt_settings_missing();
		}
		else
			die ('Script "install.php" is missing. Upload to server and ensure read/write privileges and try again.');
	}

	if(isset($_POST['login'])) {
		
		$post_params = '';
		$post_params .= 'key=' .urlencode(base64_encode(TT_SERVER_KEY)) .'&';
		$post_params .= 'passwd=' .urlencode(base64_encode($_POST['password'])); 

		$url = TT_SERVICE_HOST .'user/auth/' .$_POST['username'];
		
		$c = curl_init($url);
		curl_setopt($c, CURLOPT_POST, true); 
		curl_setopt($c, CURLOPT_POSTFIELDS, $post_params);
		curl_setopt($c, CURLOPT_HEADER, false); 
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

		$response = json_decode(curl_exec($c), true);
		curl_close($c);

		//var_dump($response);
		if(isset($response['id'])) {
			session_start();
			$_SESSION['id'] = $response['id'];
			$_SESSION['username'] = $response['username'];
			$_SESSION['last_login'] = $response['last_login'];
		}
		else {
			echo "Incorrect username or password. Try again.";
		}
	}
	
	if(!isset($_SESSION)) {
		session_start();
	}
	if(isset($_SESSION['id'])) {
		include_once('./views/welcome.php');
		die();
	}

	include_once('./views/login.php');
?>
