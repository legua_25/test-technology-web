<?php
	if(!isset($_SESSION)) {
		session_start();
	}

	// Get user's tests from server.	

	require_once('./settings.php');
	
	$post_params = 'key=' .urlencode(base64_encode(TT_SERVER_KEY)); 
	
	$url = TT_SERVICE_HOST .'surveys/all'; 
	$c = curl_init($url); 
	curl_setopt($c, CURLOPT_POST, true); 
	curl_setopt($c, CURLOPT_POSTFIELDS, $post_params);
	curl_setopt($c, CURLOPT_HEADER, false); 
	curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

	$response = json_decode(curl_exec($c), true);
	curl_close($c);
	//var_dump($response);
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>Welcome - Test Technology</title>	
		<link rel="stylesheet" type="text/css" href="login.css">
	</head>
	<body>
		<p><a href="./logout.php">Logout</a></p>
		<article>
		<div class="login">
		<h1>Test Technology</h1>
		<p>Welcome <?php echo $_SESSION['username']; ?>.</p>
		<a href="./editProfile.php">Change password</a>
		<p>This are all the surveys you have added:</p>
<?php
		if($response == NULL) {
			echo '<p>You haven\'t created any test yet.</p>';
		}
		else {
			foreach($response as $survey) {
				echo '<div class="survey_list">';
				echo '<h4>' .$survey['title'] .'</h4>';
				echo '<p>' .$survey['description'] .'</p>';
				echo '<p>Multiple attempts: ' .$survey['multiple_attempts'] .'</p>';
				echo '<p><a href="./deleteTest.php?test=' .$survey['id'] .'">Delete test</a></p>';
				echo '</div>';
			}
		}
?>
		<a href="./newTest.php">Create new test</a>
		</div>
		</article>
	</body>
</html>
