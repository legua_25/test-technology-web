<!DOCTYPE HTML>
<html>
	<head>
		<script type="text/JavaScript" src="./scripts/newTestForm.js"></script>
		<link rel="stylesheet" type="text/css" href="login.css">	
		<title>New test - Test Technology</title>
	</head>
	<body>
		<article>
		<div class="login1">
		<h1>New test</h1>
		<form method="POST" action="./newTest.php">
			<table>
				<tr><td>Title:</td><td><input type="text" name="title" id="title" required></td></tr>
				<tr><td>Description:</td><td><textarea name="description" id="description"></textarea></td></tr>
				<tr><td>Multiple attempts:</td><td><input type="checkbox" name="multiple_attempts" id="multiple_attempts"></td></tr>
			</table>
			<div class="login2" id="questions" name="questions" class="login">
				<h1>Questions</h1>
			</div>
			<table>
				<tr>
					<td>Question type:</td>
					<td><select name="question_type" id="question_type"><option name="text" id="text">Text</option><option name="radio" id="radio">Radio</option></select></td>
					<td><button type="button" name="newQuestionButton" id="newQuestionButton" onclick="newQuestion()">New question</button></td></tr>
			</table>
			<hr/>
			<input type="submit" name="newTest" id="newTest" value="Add test">
		</form>
		</div>
		</article>
	</body>
</html>
