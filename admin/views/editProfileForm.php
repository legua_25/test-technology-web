<!DOCTYPE HTML>
<html>
	<head>
		<title>Change password - Test Technology</title>
		<link rel="stylesheet" type="text/css" href="login.css">
	</head>
	<body>
		<article>
		<div class="login">
		<h1>Change password</h1>
		<form method="POST" action="./editProfile.php">
			<table>
				<tr>
					<td>New password:</td><td><input type="password" name="password" id="password" required /></td>
				</tr>
				<tr>
					<td>Confirm password:</td><td><input type="password" name="password2" id="password2" required /></td>
				</tr>
				<tr>
					<td></td><td><input type="submit" name="editProfile" id="editProfile" value="Edit profile"></td>
				</tr>
			</table>
		</form>
		</div>
		</article>
	</body>
</html>
