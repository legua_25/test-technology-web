<!DOCTYPE HTML>
<html>
	<head>
		<title>New user - Test Technology</title>
		<link rel="stylesheet" type="text/css" href="login.css">	
	</head>
	<body>
		<article>
		<div class="login">
		<h1>New user</h1>
		<p>Already signed up? Log in <a href="./">here</a>.</p>
		<form method="POST" action="">
			<table>
				<tr>
					<td>Username:</td><td><input type="text" name="username" id="username" required />*</td>
				</tr>
				<tr>
					<td>Email:</td><td><input type="email" name="email" id="email" required />*</td>
				</tr>
				<tr>
					<td>Password:</td><td><input type="password" name="password" id="password" required />*</td>
				</tr>
				<tr>
					<td>Confirm password:</td><td><input type="password" name="password2" id="password2" required />*</td>
				</tr>
				<tr>
					<td></td><td><input type="submit" name="newUser" id="newUser" value="Add user"></td>
				</tr>
			</table>
			<p>Fields marked with * are required.</p>
		</form>
		</div>
		</article>
	</body>
</html>
