<!DOCTYPE HTML>
<html>
	<head>
		<title>Login - Test Technology</title>
		<link rel="stylesheet" type="text/css" href="login.css">
	</head>
	<body>
		<article>
		<div class="login">
		<h1>Login</h1>
		<p>If you don't have an account yet, you can sign up <a href="./newUser.php">here</a>.</p>
		<form method="POST" action="">
			<table>
				<tr>
					<td>Username:</td><td><input type="text" name="username" id="username" required /></td>
				</tr>
				<tr>
					<td>Password:</td><td><input type="password" name="password" id="password" required /></td>
				</tr>
				<tr>
					<td></td><td><input type="submit" name="login" id="login" value="Login"></td>
				</tr>
			</table>
		</form>
		</div>
		</article>
	</body>
</html>
