<?php
	if(!isset($_SESSION)) {
		session_start();
	}
	//var_dump($_SESSION);

	require_once('settings.php');


	// POST from newTestForm.php
	if(isset($_POST['newTest'])) {

		$err = false;
		
		// Add basic test info first.
		$post_params = 'key=' .urlencode(base64_encode(TT_SERVER_KEY)) .'&';
		$post_params .= 'title=' .$_POST['title'] .'&';
		$post_params .= 'description=' .$_POST['description'] .'&';
		if(isset($_POST['multiple_attempts'])) {
			$post_params .= 'multiple_attempts=1';
		}
		else {
			$post_params .= 'multiple_attempts=0';
		}
		//var_dump($post_params);
		
		$url = TT_SERVICE_HOST .'survey/add';

		$c = curl_init($url);
		curl_setopt($c, CURLOPT_POST, true); 
		curl_setopt($c, CURLOPT_POSTFIELDS, $post_params);
		curl_setopt($c, CURLOPT_HEADER, false); 
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

		$response = json_decode(curl_exec($c), true);
		curl_close($c);

		//var_dump($response);
		if(!$response) {
			echo "The test couldn't be added to the database. Sorry for the inconvenience.";
			$err = true;
			include_once('./views/newTestForm.php');
			exit();
		}
		//var_dump($_POST);
		
		// Add each question.
		$i = 1;
		while(isset($_POST['question_' .$i])) {
			$question = array();
			$question['flavor-text'] = $_POST['question_' .$i];
			$question['key'] = urlencode(base64_encode(TT_SERVER_KEY));
			$question['survey-id'] = $response['id'];
			if($_POST['question_type_' .$i] == "True") {
				$question['type-id'] = 1;
			}
			else {
				$question['type-id'] = 0;
			}
			if(isset($_POST['required_' .$i])) {
				$question['required'] = 1;
			}
			else {
				$question['required'] = 0;
			}
			// Add each option.
			$question['option'] = array();
			$j = 1;
			while(isset($_POST['option' .$i .'_' .$j])) {
				$option = array();
				$option['flavor-text'] = $_POST['option' .$i .'_' .$j];
				if(!isset($_POST['value_' .$i .'_' .$j])) {
					$option['value'] = 0;
				}
				else {
					$option['value'] = $_POST['value_' .$i .'_' .$j];
				}
				if(!isset($_POST['feedback_' .$i .'_' .$j])) {
					$option['feedback'] = 0;
				}
				else {
					$option['feedback'] = $_POST['feedback_' .$i .'_' .$j];
				}
				$j++;
				array_push($question['option'], $option);
			}
			//var_dump($question);
			
			$key = urlencode(base64_encode(TT_SERVER_KEY)) .'&';

			$url = TT_SERVICE_HOST .'question/add';

			$c = curl_init($url);
			curl_setopt($c, CURLOPT_POST, true); 
			curl_setopt($c, CURLOPT_POSTFIELDS, 'key=' .$key .'data=' .json_encode($question));
			curl_setopt($c, CURLOPT_HEADER, false); 
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

			$response = json_decode(curl_exec($c), true);
			curl_close($c);

			//var_dump($response);
			if(!$response) {
				//echo "<br/>Question " .$i ." couldn't be added to the database. Sorry for the inconvenience.";
				//$err = true;
			}
			$i++;
		}
		
		
	}
	
	if(isset($err) && !$err) {
		include_once('./views/welcome.php');
	}
	else {
		include_once('./views/newTestForm.php');
	}
?>
