<?php
	require_once('settings.php');
	
	if(!isset($_SESSION)) {
		session_start();
		//var_dump($_SESSION);
	}

	// POST from editProfileForm.php
	if(isset($_POST['editProfile'])) {
		// Validate parameters.
		if($_POST['password'] != $_POST['password2']) {
			echo "Both passwords have to be the same. Try again.";
		}
		else {
			$post_params = '';
			$post_params .= 'key=' .urlencode(base64_encode(TT_SERVER_KEY)) .'&';
			$post_params .= 'id=' .$_SESSION['id'] .'&'; 
			$post_params .= 'user=' .$_SESSION['username'] .'&'; 
			$post_params .= 'passwd=' .urlencode(base64_encode($_POST['password']));

			$url = TT_SERVICE_HOST .'user/save';
	
			$c = curl_init($url);
			curl_setopt($c, CURLOPT_POST, true); 
			curl_setopt($c, CURLOPT_POSTFIELDS, $post_params);
			curl_setopt($c, CURLOPT_HEADER, false); 
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

			$response = json_decode(curl_exec($c), true);
			curl_close($c);

			var_dump($response);
	
			if($response) {
				echo "Profile has been successfully edited!";
				include_once('./views/welcome.php');
				exit();
			}
			else {
				echo "There was an error while updating the profile. Sorry for the inconvenience.";
			}
		}
	}
	
	include_once('./views/editProfileForm.php');
	
?>
