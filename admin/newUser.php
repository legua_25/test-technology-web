<?php
	require_once('settings.php');

	// POST from newUserForm.php
	if(isset($_POST['newUser'])) {
	
		// Validate required input.
		if(empty($_POST['username']) || empty($_POST['email']) || empty($_POST['password'])) {
			echo "Error: Username, email and password canntot be empty.";
			exit();
		}
		if(strcmp($_POST['password'], $_POST['password2'])) {
			echo "Error: Different passwords were provided.";
			exit();
		}
		else {
			$post_params = '';
			$post_params .= 'key=' .urlencode(base64_encode(TT_SERVER_KEY)) .'&';
			$post_params .= 'user=' .$_POST['username'] .'&';
			$post_params .= 'email=' .$_POST['email'] .'&';
			$post_params .= 'passwd=' .urlencode(base64_encode($_POST['password'])); 

			$url = TT_SERVICE_HOST .'user/add';
		
			$c = curl_init($url);
			curl_setopt($c, CURLOPT_POST, true); 
			curl_setopt($c, CURLOPT_POSTFIELDS, $post_params);
			curl_setopt($c, CURLOPT_HEADER, false); 
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

			$response = json_decode(curl_exec($c), true);
			curl_close($c);

			if($response) {
				echo $_POST['username'] ." has been added succesfully.";
				session_start();
				$_SESSION['id'] = $response['id'];
				$_SESSION['username'] = $_POST['username'];
				include_once('./views/welcome.php');
				exit();
			}
		}
	}
	
	include_once('./views/newUserForm.php');
?>
