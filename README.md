# Test Technology
## Web service and adminsitration interface
---------------------------------------------------------
This repository contains all the required assets and scripts to install and configure the Test Technology web service, API and client frontend in your server. The web service contains both the administration interface and the web service API. The client contains only the administration interface, ideal to deploy on different servers.

## Install
---------------------------------------------------------
### Dependencies
* PHP 5.3.0 or higher
* MySQL Server 5.4 or higher

### Web service installation
Pending...

### Client installation
Pending...

## Downloads
Pending...